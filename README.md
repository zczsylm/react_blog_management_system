# react博客管理系统

#### 介绍
整个项目分割为三个项目
react + antd +redux 构建后台管理系统
next + hooks + react 构建 前端个人博客页面
express + mongodb 构建后台

预览地址  www.zczsylm.top

#### 软件架构
react + antd +redux 构建后台管理系统
next + hooks + react 构建 前端个人博客页面
express + mongodb 构建后台


#### 安装教程

1.  git clone 
2.  cnpm i 
3.  yarn start

#### 使用说明

1.  本系统只开放了后台管理系统
2.  其他两个系统代码付费(99)
3.  提供购买后的协助


#### 码云特技
