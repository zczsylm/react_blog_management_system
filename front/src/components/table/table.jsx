// table 分页 jsx

import React, { Component } from 'react';
import { Table, Tag, Space, Button, Modal } from "antd";
import { withRouter } from 'react-router-dom' 

class TableC extends Component {
    constructor(props) {
        super(props);
        this.state = {
          total:0,
          pageSize:5
        };
      }
    render() {
        return (
            <div>
                <Table
          bordered
          columns={this.props.columns}
          dataSource={this.props.dataSource}
          pagination={{
            pageSize: this.state.pageSize, //每页几条数据
            total: Number(this.props.total), //多少条数据，默认给你分配多少页
          }}
        />
            </div>
        );
    }
}

export default TableC;