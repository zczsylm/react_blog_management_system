import React, { Component } from "react";
import "./index.less";
import { Layout, Menu, Breadcrumb, Avatar, Button } from "antd";
import store from "../../store";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  PoweroffOutlined,
  FileWordOutlined,
  CopyOutlined,
  EditOutlined,
  UserOutlined,
  InfoCircleOutlined,
  BellOutlined,
  UnorderedListOutlined,
  PictureOutlined,
  SettingOutlined,
  NotificationOutlined,
  ToolOutlined,
  TeamOutlined,
  ScissorOutlined,
  HighlightOutlined,
  FontSizeOutlined,
  SnippetsOutlined,
} from "@ant-design/icons";
import {
  HashRouter as Router,
  Route,
  Switch,
  withRouter,
  Redirect,
} from "react-router-dom";
import { privateRoutes, InitPrivate } from "../../router";
//引入兼容包
// import { Icon } from "@ant-design/compatible";
const { Header, Sider, Content } = Layout;
const { SubMenu } = Menu;

const mapper = {
  "file-word": FileWordOutlined,
  copy: CopyOutlined,
  edit: EditOutlined,
  user: UserOutlined,
  info: InfoCircleOutlined,
  bell: BellOutlined,
  "unordered-list": UnorderedListOutlined,
  picture: PictureOutlined,
  setting: SettingOutlined,
  notification: NotificationOutlined,
  tool: ToolOutlined,
  team: TeamOutlined,
  noteinfo:ScissorOutlined,
  editnote:HighlightOutlined,
  pictureinfo:FontSizeOutlined,
  editpicture:SnippetsOutlined
};
class LayOut extends Component {
  constructor(props) {
    super(props);
    this.state = store.getState();

    store.subscribe(this.storeChange);
    //改变标题栏目
    document.title = "admin"
    //监听页面刷新
    // window.onbeforeunload = function(e){
    //     localStorage.setItem('history')
    // };
  }

  storeChange = () => {
    this.setState(store.getState, function () {});
  };
  state = {
    collapsed: false,
    opensub:'sub0'
  };
  componentWillMount() {
    InitPrivate();
  }
  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  mianbao = () => {
    var that = this;
    {
      var a = that.state.AuthorityList.map(function (value, index) {
        if (
          !value.isTop &&
          value.pathname === that.props.history.location.pathname
        ) {
          return (
            <Breadcrumb
              style={{
                marginLeft: "25px",
                marginTop: "20px",
                fontSize: "16px",
                fontWeight: "bolder",
              }}
            >
              <Breadcrumb.Item>控制台</Breadcrumb.Item>
              <Breadcrumb.Item>{value.title}</Breadcrumb.Item>
            </Breadcrumb>
          );
        } else {
          var brr = value.children.map(function (value1) {
            if (value1.pathname === that.props.history.location.pathname) {
              return (
                <Breadcrumb
                  style={{
                    marginLeft: "25px",
                    marginTop: "20px",
                    fontSize: "16px",
                    fontWeight: "bolder",
                  }}
                >
                  <Breadcrumb.Item>控制台</Breadcrumb.Item>
                  <Breadcrumb.Item>{value.title}</Breadcrumb.Item>
                  <Breadcrumb.Item>{value1.title}</Breadcrumb.Item>
                </Breadcrumb>
              );
            }
          });
          return brr;
        }
      });
    }
    var b = a.filter(function (item) {
      return (
        Array.isArray(item) === false ||
        item[0] !== undefined ||
        item[1] !== undefined
      );
      //&&  ((item[0] !== undefined) || (item[1] !== undefined))
    });
    let obj = {};
    if (!Array.isArray(b[0])) {
      return b[0];
    } else {
      var c = b[0].forEach(function (item) {
        if (item) {
          obj = item;
        }
      });
      return obj;
    }
  };
  // 跳转路由
  TrunTo = ({ item, key, keyPath, domEvent }) => {
    this.props.history.push(key);
  };
  LoginOut = () => {
    localStorage.clear();
    this.props.history.push("/login");
  };
  MI = (item) => {
    var Temp = mapper[item];
    return <Temp />;
  };
  onSelectkey = (item, key, keyPath, selectedKeys, domEvent)=>{
    console.log(item, key, keyPath, selectedKeys, domEvent)
  }
  //定位到默认打开的submenu
  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };
  render() {
    return (
      <Layout className="container">
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          <div
            className="logo"
            style={{
              color: "white",
              fontSize: "25px",
              fontWeight: "bolder",
              lineHeight: "50px",
            }}
          >
            CMS
          </div>
          <Menu
            theme="dark"
            mode="inline"
            defaultSelectedKeys={[this.props.location.pathname]}
            defaultOpenKeys={[this.state.opensub]}
            onClick={({ item, key, keyPath, domEvent }) => {
              this.TrunTo({ item, key, keyPath, domEvent });
            }}
          >
            {this.state.AuthorityList.map((item, index) => {
              if (item.isTop) {
                return (
                  <SubMenu
                    key={"sub" + index}
                    title={
                      <span>
                        {/* <Icon type={item.icon} /> */}
                        <span>{item.title}</span>
                      </span>
                    }
                    icon={this.MI(item.icon)}
                  >
                    {item.children.map((item1, index1) => {
                      return (
                        <Menu.Item
                          key={item1.pathname}
                          icon={this.MI(item1.icon)}
                        >
                          {/* <Icon type={item1.icon} /> */}
                          <span>{item1.title}</span>
                        </Menu.Item>
                      );
                    })}
                  </SubMenu>
                );
              } else {
                return (
                  <Menu.Item key={item.pathname} icon={this.MI(item.icon)}>
                    {/* <Icon type={item.icon} /> */}
                    <span>{item.title}</span>
                  </Menu.Item>
                );
              }
            })}
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
              fontSize: "25px",
              paddingLeft: "25px",
              backgroundColor: "#001529",
            }}
          >
            {React.createElement(
              this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: this.toggle,
              }
            )}
            <div className="loginout">
              <Avatar
                style={{
                  backgroundColor: "#87d068",
                  marginRight: "20px",
                  width: "45px",
                  height: "45px",
                  lineHeight: "45px",
                  marginBottom: "5px",
                }}
                icon={
                  <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                }
              />
              <Button
                type="danger"
                className="outbutton"
                size="large"
                onClick={this.LoginOut}
              >
                {/* <Icon type="poweroff" style={{ fontSize:'20px',fontWeight:'bolder',lineHeight:'10px'}} /> */}
                <PoweroffOutlined
                  style={{
                    fontSize: "20px",
                    fontWeight: "bolder",
                    lineHeight: "10px",
                  }}
                />
              </Button>
            </div>
          </Header>
          {this.mianbao()}

          {/* <Breadcrumb.Item>Home</Breadcrumb.Item> */}
          <Content
            className="site-layout-background"
            style={{
              margin: "24px 16px",
              padding: 24,
              minHeight: 280,
            }}
          >
            <Switch>
              {privateRoutes.map((item, index) => {
                return (
                  <Route
                    path={item.pathname}
                    key={index}
                    render={() => {
                      var Temp = item.component;
                      return this.state.token ? (
                        <Temp />
                      ) : (
                        <Redirect to="/toAuth" />
                      );
                    }}
                    // component={item.component}
                  ></Route>
                );
              })}
              <Redirect from="/admin" to="/admin/info" exact></Redirect>
              <Redirect to="/toAuth"></Redirect>
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

export default withRouter(LayOut);
