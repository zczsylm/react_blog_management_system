/*
 * @Descripttion: 
 * @Author: sueRimn
 * @Date: 2020-07-07 09:07:44
 */ 
//根组件
import React, { Component } from "react";

import {
  HashRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
//引入自定义模块
import { commonRoutes } from "./router";

import LayOut from "./components/layout";

export default class App extends Component {
  constructor(props){
    super(props)
  }
  render() {
    return (
      <Router>
        <Switch>
          {commonRoutes.map((item, index) => {
            //配置公共路由
            return (
              <Route
                key={index}
                path={item.pathname}
                component={item.component}
              ></Route>
            );
          })}
          <Route path="/admin" component={LayOut}></Route>
          <Redirect to="/404"></Redirect>
        </Switch>
      </Router>
    );
  }
}
