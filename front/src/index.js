/*
 * @Descripttion: 
 * @Author: sueRimn
 * @Date: 2020-07-07 09:07:44
 */ 
//入口js
import React from 'react'
import ReactDom from 'react-dom'
import App from './App'
//引入antd-css
import 'antd/dist/antd.css';
ReactDom.render(<App />,document.getElementById('root'))