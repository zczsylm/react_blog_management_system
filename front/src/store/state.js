/*
 * @Descripttion: 
 * @Author: sueRimn
 * @Date: 2020-07-11 09:41:11
 */ 
import crypt from '../utils/encrypt'
export default {
    //state的默认值
    // userinfo:{
        // username: crypt.Decrypt(localStorage.getItem('username')) || '',
        // password: crypt.Decrypt(localStorage.getItem('password')) || ''
        // username:'',
        // password:''
    // },
    uid:'' || crypt.decrypt(localStorage.getItem('uid')),
    token: '' || crypt.decrypt(localStorage.getItem('token')),
    AuthorityList: []
}