/*
 * @Descripttion: 
 * @Author: sueRimn
 * @Date: 2020-07-11 09:14:27
 */ 
import { GET_AUTHORITY, LOGIN_SUCCESS } from "./actiontypes";
import { Authority } from "../api/index";
import crypt from '../utils/encrypt'
export const  ToGetAuthority = () => {
    return dispatch => {
    Authority().then((res) => {
      try{
        const action = GetAuthority(res[0].items);
        localStorage.setItem('AuthorityList',crypt.encrypt(res[0].items))
        dispatch(action);
      }catch(e){

      }
        
    });
  };
};
export const GetAuthority = (value) => ({
    type: GET_AUTHORITY,
    value
})
// export const SaveUser = (value) => ({
//   type: SAVE_USER,
//   value,
// });
export const LoginSuccess = (value) => ({
  type: LOGIN_SUCCESS,
  value,
});
