/*
 * @Descripttion: 
 * @Author: sueRimn
 * @Date: 2020-07-11 09:13:58
 */ 
import { GET_AUTHORITY,LOGIN_SUCCESS } from './actiontypes'
//加密
//引入默认值
import defaultState from './state'
import crypt from '../utils/encrypt'
export default (state = defaultState,action)=>{
    switch(action.type){
        // case SAVE_USER:
        //     //深拷贝
        //     {
        //         let newState = JSON.parse(JSON.stringify(state))
        //         newState.userinfo = action.value
        //         return newState;
        //     }
        case LOGIN_SUCCESS:
            {
                // localStorage.setItem("username",action.value.username);
                // localStorage.setItem("password",action.value.password);
                let newState = JSON.parse(JSON.stringify(state))
                newState.token = action.value.token
                newState.uid = action.value.uid
                localStorage.setItem("token",crypt.encrypt(action.value.token));
                localStorage.setItem("uid",crypt.encrypt(action.value.uid));
                return newState;
            }
        case GET_AUTHORITY:
            {
                let newState = JSON.parse(JSON.stringify(state))
                newState.AuthorityList = action.value
                // localStorage.setItem("AuthorityList",JSON.stringify(action.value));
                return newState;
            }
        default:
            return state
    }
}