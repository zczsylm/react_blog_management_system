//仓库
import { createStore,applyMiddleware,compose } from "redux";
//引入reducer
import reducer from "./reducer";
//创建了一个仓库
//然后注入reducer
// import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk'
// const sagaMiddleWare = createSagaMiddleware();
const composeEnhances = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}):compose
const enhancer = composeEnhances(applyMiddleware(thunk))
const store = createStore(
  reducer,
  enhancer
);
export default store;