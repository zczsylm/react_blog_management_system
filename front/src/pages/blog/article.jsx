import React, { Component } from "react";
import { Table, Tag, Space, Button, Modal } from "antd";
//引入日期格式化
import moment from "moment";
import { AllBlog, DeleteBlog } from "../../api";
import { withRouter } from 'react-router-dom' 
//引入复用组件
import TableC from '../../components/table/table'
class Article extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "序号",
          dataIndex: "name",
          render: (text, record, index) => `${index + 1}`,
        },
        {
          title: "标题",
          dataIndex: "title",
          ellipsis: true,
        },
        {
          title: "标签",
          dataIndex: "tag",
        },
        {
          title: "创建时间",
          dataIndex: "create_time",
        },
        {
          title: "更新时间",
          dataIndex: "update_time",
        },
        {
          title: "评论",
          dataIndex: "commets",
        },
        {
          title: "浏览",
          dataIndex: "view",
        },
        {
          title: "操作",
          dataIndex: "operate",
          render: (text, record) => (
            <span>
              <Button
                type="primary"
                style={{ marginLeft: "10px",marginBottom:'10px' }}
                onClick={() => {
                  this.EditItem(record);
                }}
              >
                编辑
              </Button>
              <Button
                type="primary"
                danger
                style={{ marginLeft: "10px" }}
                onClick={() => {
                  this.DelItem(record);
                }}
              >
                删除
              </Button>
            </span>
          ),
        },
      ],
      data: [],
      total:0,
      pageSize:5
    };
  }
  DelItem = (record) => {
    var _this = this;
    DeleteBlog({
      id: record._id,
    }).then((res) => {
      Modal.info({
        centered: true,
        content: (
          <div>
            <h1>操作成功</h1>
          </div>
        ),
        onOk() {
          _this.getInfo();
        },
      });
    });
  };
  EditItem = (record) => {
    this.props.history.push({pathname:"/admin/tag",query: { id: record._id }})
  };
  getInfo = () => {
    moment.locale("zh-cn");
    AllBlog().then((res) => {
      var total = res.length
      this.setState({
        total
      }) 
      res.forEach((item, index) => {
        if (item.create_time) {
          item.create_time = moment(item.create_time)
            .utc(8)
            .format("YYYY/MM/DD HH:mm:ss");
          item.update_time = moment(item.update_time)
            .utc(8)
            .format("YYYY/MM/DD HH:mm:ss");
        }
        item.key = item._id;
      });
      this.setState({
        data: res,
      },function(){
      });
    });
  };
  componentDidMount() {
    this.getInfo();
  }
  render() {
    return (
      <div>
        <TableC
          total={this.state.total} 
          columns={this.state.columns}
          dataSource={this.state.data} />
      </div>
    );
  }
}

export default withRouter(Article);
