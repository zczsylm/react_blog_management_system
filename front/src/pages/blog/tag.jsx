import React, { Component } from "react";
import Editor from "for-editor";
import { Input, Select, Button, Modal } from "antd";
import { AddBlog, BlogArticle,UpdateBlog } from "../../api";
import { withRouter } from "react-router-dom";
const { Option } = Select;

class Tag extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: "",
      title: "",
      tag: "",
    };
  }
  componentDidMount() {
    if (this.props.location.query) {
      var _id = this.props.location.query.id || undefined;
      if (_id) {
        BlogArticle({ id: _id }).then((res) => {
          this.setState({
            content:res[0].content,
            title:res[0].title,
            tag:res[0].tag
          })
        });
      }
    }
  }
  handleChange = (content) => {
    //返回输入的值
    this.setState({
      content,
    });
  };
  SaveChange = () => {
    //返回输入的值
    //判断是提交还是修改
    if (this.props.location.query) {
      //修改
      UpdateBlog({
          id:this.props.location.query.id,
          content:this.state.content,
          title:this.state.title,
          tag:this.state.tag
      }).then(res=>{
        Modal.success({
          content: '修改成功~~~',
          centered: true,
        });
      })
    }else{
      if (this.state.content && this.state.title && this.state.tag) {
        AddBlog(this.state)
          .then((res) => {
            Modal.success({
              content: '提交成功~~~',
              centered: true,
            });
          })
          .catch((err) => {});
      } else {
        Modal.error({
          title: "错误信息",
          content: "标题内容不能为空",
          centered: true,
        });
      }
    }
  };
  //选择输入框
  changetag = (e) => {
    this.setState({
      tag: e.target.value,
    });
  };
  //标题
  changetitle = (e) => {
    this.setState({
      title: e.target.value,
    });
  };
  render() {
    return (
      <div>
        <div
          style={{
            paddingLeft: "20%",
            fontSize: "18px",
            marginBottom: "20px",
            display: "flex",
          }}
        >
          <div style={{ display: "flex" }}>
            <span style={{ width: "60px" }}>标题:</span>
            <Input value={ this.state.title } placeholder="请输入标题" onChange={this.changetitle} />
          </div>
          <div style={{ display: "flex", marginLeft: "50px" }}>
            <span style={{ width: "60px" }}>分类:</span>
            <Input value={ this.state.tag } placeholder="请输入分类" onChange={this.changetag} />
          </div>
          <div>
            <Button
              type="primary"
              style={{ marginLeft: "150px" }}
              onClick={(e) => this.SaveChange(e)}
            >
              提交
            </Button>
          </div>
        </div>
        <Editor
          lineNum={true}
          preview={true}
          subfield={true}
          value={this.state.content}
          onChange={(value) => this.handleChange(value)}
          onSave={() => this.SaveChange()}
        />
      </div>
    );
  }
}

export default withRouter(Tag);
