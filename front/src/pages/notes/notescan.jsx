import React, { Component } from "react";
import Editor from "for-editor";
import { Input, Select, Button, Modal } from "antd";
import { AddNote } from "../../api";
import { withRouter } from "react-router-dom";
import UploadUtil from "../../components/upload/upload";
const { Option } = Select;

class NoteScan extends Component {
  constructor() {
    super();
    this.state = {
      content: "",
      title: "",
    };
  }
  Submit = (data) => {
    return new Promise((resolve, reject) => {
      AddNote(data)
        .then((res) => {
          resolve(res)
        })
        .catch((err) => {
          reject(err)
        });
    });
  };
  componentDidMount() {}
  render() {
    return (
      <div>
        <UploadUtil Submit={this.Submit} />
      </div>
    );
  }
}

export default withRouter(NoteScan);
