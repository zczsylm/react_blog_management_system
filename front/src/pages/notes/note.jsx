import React, { Component } from "react";
import { Table, Tag, Space, Button, Modal } from "antd";
//引入日期格式化
import moment from "moment";
import { withRouter } from "react-router-dom";
import { AllNote, DeleteArticle } from "../../api";
import { Card } from "antd";
const { Meta } = Card;
class Note extends Component {
  constructor(props) {
    super(props);
    this.state = {
      noteinfo: [],
    };
  }
  componentDidMount() {
    AllNote().then((res) => {
      this.setState({
        noteinfo: res,
      });
    });
  }
  render() {
    return (
      <div style={{ display:'flex' }}>
        {this.state.noteinfo.map((item,index) => {
          return (
            <Card
            key={index}
              hoverable
              style={{ width: 240,marginLeft:'25px' }}
              cover={
                <img
                  alt="example"
                  src={"http://47.101.212.62:4000/image/" + item.imgid }
                />
              }
            >
              <Meta
                title={item.filename}
                description={item.description}
              />
            </Card>
          );
        })}
      </div>
    );
  }
}

export default withRouter(Note);
