import React, { Component } from "react";
import { Table, Tag, Space, Button, Modal } from "antd";
import { UserAll } from "../../api";
import { withRouter } from "react-router-dom";
import moment from "moment";
//引入复用组件
import TableC from '../../components/table/table'
class Front extends Component {
  constructor(props) {
    super(props);
    this.state = {
      columns: [
        {
          title: "序号",
          dataIndex: "name",
          render: (text, record, index) => `${index + 1}`,
        },
        {
          title: "姓名",
          dataIndex: "username",
        },
        {
          title: "邮箱",
          dataIndex: "email",
        },
        {
          title: "年龄",
          dataIndex: "age",
        },
        {
          title: "创建时间",
          dataIndex: "create_time",
        },
        {
          title: "操作",
          dataIndex: "operate",
          render: (text, record) => (
            <span>
              <Button
                type="primary"
                style={{ marginLeft: "10px", marginBottom: "10px" }}
                onClick={() => {
                  this.EditItem(record);
                }}
              >
                编辑
              </Button>
            </span>
          ),
        },
      ],
      data: [],
    };
  }
  //获取用户信息
  getInfo = () => {
    UserAll().then((res) => {
      res.forEach((item, index) => {
        if (item.create_time) {
          item.create_time = moment(item.create_time)
            .utc(8)
            .format("YYYY/MM/DD HH:mm:ss");
          item.update_time = moment(item.update_time)
            .utc(8)
            .format("YYYY/MM/DD HH:mm:ss");
        }
        item.key = item._id;
      });
      this.setState({
        data: res,
      });
    });
  };
  componentDidMount() {
    this.getInfo();
  }
  EditItem = (record) => {
    this.props.history.push({
      pathname: "/admin/back",
      query: { id: record._id },
    });
  };
  render() {
    return (
      <div>
        <TableC
         total={this.state.total} 
         columns={this.state.columns}
         dataSource={this.state.data}
         />
      </div>
    );
  }
}

export default withRouter(Front);
