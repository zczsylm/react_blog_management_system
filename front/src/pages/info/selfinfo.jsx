import React, { Component } from "react";
import { Form, Input, Button,Modal } from "antd";
import "./selfinfo.less";
import { UpdateUser,UserInfo } from '../../api'
import { 
  ExclamationCircleOutlined
 } from '@ant-design/icons'
import store from '../../store'

// 
class SelfInfo extends Component {
  //创建实例
  constructor(props){
    super(props)
    this.state = {
      userinfo:{},
      visible: false
    }
  }
  formRef = React.createRef();
  onFinish = (values) => {
  values.uid = store.getState().uid
    UpdateUser(values).then(()=>{
      this.setState({
        visible:false
      },function(){
        this.showModal()
      })
    })
  };
  // initialValues = this.state.userinfo
  onReset = () => {
    this.formRef.current.resetFields();
  };
  componentDidMount(){
    this.getInfo()
  }
  getInfo(){
    UserInfo({uid:store.getState().uid}).then((res)=>{
      this.formRef.current.setFieldsValue(res[0])
    })
  }
  validateMessages = {
    required: '${label}是必填项',
    pattern:{
      mismatch: "${label} 没有匹配正则"
    },
    string:{
      range: "${label}必须大于${min}小于${max}个字符",
    },
    number:{
      range: "${label}必须大于${min}小于${max}",
    }
  };
  showModal = () => {
    this.setState({
      visible: true,
    });
  };

  hideModal = () => {
    this.setState({
      visible: false,
    });
  };
  render() {
    return (
      <div className="content">
        <Modal
          title="提交成功"
          centered
          visible={this.state.visible}
          onOk={this.hideModal}
          onCancel={this.hideModal}
          okText="确认"
          footer={null}
          cancelText="取消"
        >
        </Modal>
        <div className="formleft">
          <h2>基本信息</h2>
          <Form
            ref={this.formRef}
            name="control-ref"
            onFinish={this.onFinish}
            validateMessages={this.validateMessages}
          >
            <Form.Item
              name="username"
              label="姓名"
              // initialValue = {this.initialValues.username}
              rules={[
                {
                  required: true,
                  min:5,
                  max:15
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="email"
              label="邮箱"
              rules={[
                {
                  required: true,
                  pattern: /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/
                },
              ]}
            >
              <Input disabled />
            </Form.Item>
            <Form.Item
              name="password"
              label="密码"
              rules={[
                {
                  required: true,
                  min:5,
                  max:15
                },  
              ]}
            >
              <Input.Password key="passwrod" />
            </Form.Item>
            <Form.Item
              name="age"
              label="年龄"
              rules={[
                {
                  min:10,
                  max:100,
                  required: true,
                  pattern:/^[0-9]*$/
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="qq"
              label="QQ"
              rules={[
                {
                  min:6,
                  max:10,
                  required: true,
                  pattern:/^[0-9]*$/
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="wechat"
              label="微信"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="avatar"
              label="头像"
              rules={[
                {
                  required: true,
                  max:200
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="github"
              label="git"
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <Input/>
            </Form.Item>
            <Form.Item
              name="description"
              label="描述"
              rules={[
                {
                  required: true,
                  min:25,
                  max:100
                },
              ]}
            >
              <Input.TextArea rows={6} />
            </Form.Item>
            <Form.Item
              name="hobby"
              label="爱好"
              rules={[
                {
                  required: true,
                  max:50,
                  min:5
                },
              ]}
            >
              <Input.TextArea rows={3}/>
            </Form.Item>
            <Form.Item>
              <Button type="primary" className="button" htmlType="submit">
                提交
              </Button>
              <Button type="primary" htmlType="button" className="button" onClick={this.onReset}>
                重置
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    );
  }
}
export default SelfInfo;
