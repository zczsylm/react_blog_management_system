import React, { Component } from "react";
// import { Chart, Line, Point } from "bizcharts";
import { Statistic, Row, Col, Button } from "antd";
import "./info.less";
import { AllNoteScan, AllBlogScan } from "../../api";
// 数据源

class Info extends Component {
  constructor(props) {
    super(props);
    this.state = {
      BlogViews: 0,
      BlogCommets: 0,
      NoteViews:0
    };
  }
  componentDidMount() {
    AllBlogScan().then((res) => {
    //   const arr = [];
    //   res.forEach((item) => {
    //     let obj = {};
    //     obj.year = item._id;
    //     obj.allview = item.totalViews;
    //     arr.push(obj);
    //   });
        if(res && res[0]){
            this.setState({
                BlogViews: res[0].totalViews || 0,
                BlogCommets: res[0].totalComments || 0,
            });
        }
    });
    AllNoteScan().then((res) => {
        if(res && res[0]){
            this.setState(
                {
                    NoteViews: res[0].totalViews || 0
                },
              );
        }
    });
  }
  render() {
    return (
      <div>
        {/* <div className="charts">
          <div className="scanchart">
            <h3>个人博客浏览量</h3>
          </div>
          <div className="scanchart">
            <h3>个人笔记浏览量</h3>
          </div>
        </div> */}
        <Row gutter={24} style={{ textAlign:'center',marginTop:'20px' }}>
          <Col span={5} >
            <Statistic title="个人博客浏览量" valueStyle={{ color: 'red' }} value={this.state.BlogViews} />
          </Col>
          <Col span={5}>
            <Statistic title="个人笔记浏览量" valueStyle={{ color: 'red' }} value={this.state.NoteViews} />
          </Col>
          <Col span={5}>
            <Statistic title="个人笔记博客评论" valueStyle={{ color: 'red' }} value={this.state.BlogCommets} />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Info;
