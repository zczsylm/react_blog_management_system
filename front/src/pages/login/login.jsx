import React, { Component } from "react";
import ReactCanvasNest from 'react-canvas-nest';
import { Input, Button, Form, message } from "antd";
import { UserOutlined, LockOutlined,PoweroffOutlined } from "@ant-design/icons";
import { LoginIn } from "../../api/index";
import { withRouter } from "react-router-dom";
//引入  actions
import store from "../../store";
import "./login.less";

import {
  LoginSuccess,
  ToGetAuthority,
} from "../../store/actioncreators";
//加密
class Login extends Component {
  constructor(props) {
    super(props);
    //store 订阅
    document.title = "博客Login"
    this.state = {
      buttontip:'登陆',
      control:false,
      userinfo: {
        username: "",
        password: "",
      },
    };
    // 页面上的内容并没有随着store的更新而更新，所以如下操作:
    // store发生改变，则subscribe()绑定的函数会自动执行
    store.subscribe(this.storeChange);
  }
  storeChange = () => {
    this.setState(store.getState);
  };
  Auth = (e, type) => {
    const obj = this.state.userinfo;
    if (type == "username") {
      obj.username = e.target.value;
    } else {
      obj.password = e.target.value;
    }
    this.setState({
      userinfo: obj,
    });
    // const action = SaveUser(obj)
    // store.dispatch(action)
  };
  Submit = () => {
    if (
      this.state.userinfo.username == "" ||
      this.state.userinfo.password == ""
    ) {
      return message.error("请输入用户名或者密码");
    }
    LoginIn(this.state.userinfo).then((res) => {
      this.setState({
        buttontip: 'loading...',
        control:true
      })
      
      if (res.errinfo) {
        this.setState({
          buttontip: '登录',
          control:false
        })
        return message.error(res.errinfo);
        
      } else {
        //加密
        // obj.username = crypt.Encrypt(this.state.userinfo.username)
        // obj.password = crypt.Encrypt(this.state.userinfo.password)
        //登陆成功存储到redux
        // let token = res.token;
        // let uid = res.uid
        const action1 = LoginSuccess(res);
        store.dispatch(action1);

        //存储到redux权限列表
        const action2 = ToGetAuthority();
        store.dispatch(action2);
        setTimeout(()=>{
          this.setState({
            buttontip: '登录',
            control:true
          })
          this.props.history.push({ pathname: "/admin" });
        },1500)
      }
    });
  };

  Regist = ()=>{
    this.props.history.push('/regist')
  }
  componentDidMount(){
    let _this = this
    const login = document.getElementById('content1')
    console.log(login)
    login.addEventListener('keypress',function(e){
      if(e.charCode === 13){
        _this.Submit()
      }
    })
  }
  render() {
    return (
      <div className="login" id="login">
        <ReactCanvasNest config = {{ pointColor: ' 236, 131, 10 ',lineColor:'236, 131, 10',lineWidth:3,count:50 ,dist:1000,pointR:2}} style={{ background:'#FFEFD5' }} />
        <div className="content1" id="content1">
          <Form>
            <div className="title">
              <h2>用户登陆</h2>
            </div>
            <div className="item">
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                size="large"
                onChange={(e) => {
                  this.Auth(e, "username");
                }}
                value={this.state.userinfo.username}
                placeholder="请输入用户名"
              />
            </div>
            <div className="item">
              <Input
                prefix={<LockOutlined />}
                // visibilityToggle={true}
                size="large"
                onChange={(e) => {
                  this.Auth(e, "password");
                }}
                type={'password'}
                placeholder="请输入密码"
                value={this.state.userinfo.password}
              />
            </div>
            <Button
              type="primary"
              className="submit"
              size={"large"}
              loading={this.state.control}
              icon={<PoweroffOutlined />}
              onClick={this.Submit}
            >
              {this.state.buttontip}
            </Button>
            <Button
              type="primary"
              size={"large"}
              className="regist_btn"
              onClick={this.Regist}
              danger
            >
              注册
            </Button>
          </Form>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
