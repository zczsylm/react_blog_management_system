import React, { Component } from "react";
import './notfound.css'
class Notfound extends Component {
  render() {
    return (
      <div style={{ marginTop:'10%' }}>
        <h1>404 Error Page</h1>
        <p class="zoom-area">
          <b>找不到她了</b>
          <b>请检查网络状况</b>
        </p>
        <section class="error-container">
          <span class="four">
            <span class="screen-reader-text">4</span>
          </span>
          <span class="zero">
            <span class="screen-reader-text">0</span>
          </span>
          <span class="four">
            <span class="screen-reader-text">4</span>
          </span>
        </section>
      </div>
    );
  }
}

export default Notfound;
