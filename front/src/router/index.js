/*
 * @Descripttion:
 * @version:
 * @Author: sueRimn
 * @Date: 2020-07-07 09:07:44
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2020-08-07 15:30:40
 */
import store from '../store/index'
import { ToGetAuthority } from "../store/actioncreators";
import Login from "../pages/login/login";
import NotFound from "../pages/NotFound/notfound";
import article from "../pages/blog/article";
import tag from "../pages/blog/tag";
import info from "../pages/info/info";
import selfinfo from "../pages/info/selfinfo";
import back from "../pages/sys/back";
import front from "../pages/sys/front";
import pic from "../pages/pic/pic";
import note from "../pages/notes/note";
import Auth from "../pages/auth/auth";
import toAuth from "../pages/auth/toauth";
import crypt from '../utils/encrypt'
import picscan from '../pages/pic/picscan'
import notescan from '../pages/notes/notescan'
import regist from '../pages/regist/regist'


//定义映射名称和组件
const mapper = {
  "/admin/note": note,
  "/admin/info": info,
  "/admin/selfinfo": selfinfo,
  "/admin/back": back,
  "/admin/front": front,
  "/admin/pic": pic,
  "/admin/auth": Auth,
  "/admin/article": article,
  "/admin/tag": tag,
  "/admin/picscan": picscan,
  "/admin/notescan": notescan
};
//公有路由
const commonRoutes = [
  {
    pathname: "/login",
    component: Login,
  },
  {
    pathname: "/404",
    component: NotFound,
  },
  {
    pathname:'/regist',
    component: regist
  },
  {
    pathname: "/toAuth",
    component: toAuth,
  },
];
//私有路由
const privateRoutes = [];
var routers = null;

function InitPrivate() {
  var AuthorityList;
  if(((!store.getState().AuthorityList.length) && localStorage.getItem('AuthorityList'))){
    AuthorityList = JSON.parse(crypt.decrypt(localStorage.getItem('AuthorityList')))
    const action2 = ToGetAuthority();
    store.dispatch(action2);
  }else{
    AuthorityList = store.getState().AuthorityList 
  }
  if (AuthorityList) {
    AuthorityList.forEach((element) => {
      if (!element.isTop) {
        let obj = {};
        obj.pathname = element.pathname;
        obj.component = mapper[element.pathname];
        obj.auth = element.auth;
        privateRoutes.push(obj);
      } else {
        element.children.forEach((item) => {
          let obj = {};
          obj.pathname = item.pathname;
          obj.component = mapper[item.pathname];
          obj.auth = element.auth;
          privateRoutes.push(obj);
        });
      }
    });
  }else{
    return;
  }
}
export { commonRoutes, privateRoutes, routers, InitPrivate };
