/*
 * @Descripttion: Api封装函数
 * @version: 1.0
 * @Author: mikasa
 * @Date: 2020-07-07 09:46:23
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2020-07-30 14:15:43
 */ 

import {getData,postData,UploadFile} from '../utils/http'

/**
 * @param {username,password} 
 */
export function LoginIn(data = {}){
    return getData('/login',data)
}
/**
 * @param {username} 
 */
export function Authority(data = {}){
    return getData('/authority',data)
}
/**
 * @param {type}
 */
export function AuthorityEdit(data = {}){
    return getData('/authority/edit',data)
}
/**
 * @param {emial_address} 
 */
export function SendEmail(data = {}){
    return getData('/user/email',data)
}
/**
 * @param {req.body username,eamil,password} 
 */
export function AddUser(data = {}){
    return getData('/user/add',data)
}
/**
 * @param {username,password,resource,emial} 
 */
export function UpdateUser(data = {}){
    return getData('/user/update',data)
}
export function UserAll(data = {}){
    return getData('/user/all',data)
}
export function UserInfo(data = {}){
    return getData('/user/userinfo',data)
}
/**
 * @param {username} 
 */
export function DeleteUser(data = {}){
    return getData('/user/delete',data)
}
//笔记
/**
 * @param {uid}
 */
export function AddNote(data = {}){
    return UploadFile('/note/uploadnote',data)
}
/**
 * @param {uid} 
 */
export function AllNote(data = {}){
    return getData('/note/allNote',data)
}
export function AllNoteScan(data = {}){
    return getData('/note/allscan',data)
}
export function AllBlogScan(data = {}){
    return getData('/blog/allscan',data)
}
/**
 * @param {id} 
 */
export function Article(data = {}){
    return getData('/note/article',data)
}
/**
 * @param {id} 
 */
export function DeleteArticle(data = {}){
    return getData('/note/delete',data)
}
/**
 * @param {id,title,content} 
 */
export function UpdateArticle(data = {}){
    return getData('/note/update',data)
}
/**
 * @param {id} 
 */
export function NoteView(data = {}){
    return getData('/note/view',data)
}
//博客
/**
 * @param {uid} 
 */
export function AddBlog(data = {}){
    return getData('/blog/add',data)
}
/**
 * @param {uid}  
 */
export function AllBlog(data = {}){
    return getData('/blog/all',data)
}
/**
 * @param {id} 
 */
export function BlogArticle(data = {}){
    return getData('/blog/article',data)
}
/**
 * @param {id} 
 */
export function DeleteBlog(data = {}){
    return getData('/blog/delete',data)
}
/**
 * @param {id,title,content} 
 */
export function UpdateBlog(data = {}){
    return getData('/blog/update',data)
}
/**
 * @param {id} 
 */
export function BlogView(data = {}){
    return getData('/blog/view',data)
}
//博客评论
/**
 * @param {uid,bid,rp_id?choose} 
 */
export function BlogCommets(data = {}){
    return getData('/commets/add',data)
}
/**
 * @param {bid} 
 */
export function AllCommets(data = {}){
    return getData('/commets/all',data)
}
/**
 * @param {rp_id} 
 */
export function SingleCommets(data = {}){
    return getData('/commets/single',data)
}

//权限修改
export function AllAuthority(data = {}){
    return getData('/authority/all',data)
}
export function UpdateAuthority(data = {}){
    return getData('/user/updateresource',data)
}

