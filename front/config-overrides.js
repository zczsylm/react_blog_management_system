const { override, fixBabelImports,addLessLoader  } = require("customize-cra");
module.exports = override(
  //对antd实现按需打包
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    // style: "css",
    style: true,
  }),
  addLessLoader({
    lessOptions: {
      modifyVars: { '@primary-color': '#1DA57A' },
      javascriptEnabled: true,
    },
  })
);
